use pyo3::create_exception;
use pyo3::prelude::*;
use std::any::Any;
use std::error::Error;
use std::rc::Rc;

create_exception!(lisa, SyntaxError, pyo3::exceptions::Exception);
create_exception!(lisa, RuntimeError, pyo3::exceptions::Exception);

#[pyclass]
struct Interpreter {
    inner: Box<lisa::interpreter::Interpreter>,
}

#[pymethods]
impl Interpreter {
    #[new]
    fn new(obj: &PyRawObject, library: pyo3::PyObject) {
        let lib = if library.is_none() {
            None
        } else {
            Some(Rc::new(Library::new(library)) as Rc<dyn lisa::libraries::Library>)
        };
        obj.init(Interpreter {
            inner: Box::new(lisa::interpreter::Interpreter::new(lib)),
        });
    }

    pub fn run(&mut self, instruction: &Instruction) -> Result<State, pyo3::PyErr> {
        let r = self.inner.run(&instruction.inner);
        let r = r.map_err(boxerror_to_pyerr);
        Ok(State::new(r?))
    }
}

#[derive(Debug)]
struct PyError(pyo3::PyErr);
impl std::fmt::Display for PyError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{:?}", self.0)
    }
}
impl std::error::Error for PyError {}
impl std::convert::Into<pyo3::PyErr> for PyError {
    fn into(self) -> pyo3::PyErr {
        self.0
    }
}

fn boxerror_to_pyerr(err: Box<dyn std::error::Error>) -> pyo3::PyErr {
    match err.downcast::<PyError>() {
        Ok(pyerr) => (*pyerr).into(),
        Err(err) => PyErr::new::<RuntimeError, _>(format!("----- {}", err)),
    }
}

fn error_to_pyerr<T>(other: &dyn Error) -> pyo3::PyErr
where
    T: pyo3::type_object::PyTypeObject,
{
    PyErr::new::<T, _>(format!("{}", other))
}

#[pyclass]
struct Instruction {
    inner: Box<lisa::instruction::Instruction>,
}
impl Instruction {
    pub fn new(inner: Box<lisa::instruction::Instruction>) -> Self {
        Instruction { inner }
    }
}
#[pymethods]
impl Instruction {
    #[staticmethod]
    pub fn parse(source: &str) -> PyResult<Option<Instruction>> {
        match lisa::parser::parse_source(source) {
            Ok(obi) => Ok(obi.map(|bi| Instruction::new(bi))),
            Err(err) => Err(error_to_pyerr::<SyntaxError>(&err)),
        }
    }
}

#[pyclass]
struct State {
    inner: Box<lisa::interpreter::State>,
}
impl State {
    pub fn new(inner: lisa::interpreter::State) -> Self {
        State {
            inner: Box::new(inner),
        }
    }
}
#[pymethods]
impl State {
    pub fn get(&self, name: &str) -> PyResult<PyObject> {
        let v = self
            .inner
            .namespace
            .get(name)
            .map_err(|e| error_to_pyerr::<pyo3::exceptions::KeyError>(&e))?;
        let gil = Python::acquire_gil();
        let py = gil.python();
        Ok(match v {
            lisa::interpreter::VariableValue::None => py.None(),
            lisa::interpreter::VariableValue::Int(i) => i.to_object(py),
            lisa::interpreter::VariableValue::Bool(b) => b.to_object(py),
        })
    }
    pub fn get_context(&self) -> PyObject {
        let gil = Python::acquire_gil();
        let py = gil.python();
        match self.inner.lib_context.downcast_ref::<pyo3::PyObject>() {
            Some(obj) => obj.clone_ref(py),
            _ => unreachable!(),
        }
    }
}

struct Library {
    inner: pyo3::PyObject,
}
impl Library {
    fn new(inner: pyo3::PyObject) -> Self {
        Library { inner }
    }
}
impl lisa::libraries::Library for Library {
    fn run_method(
        &self,
        method_name: &str,
        _arguments: &[lisa::expression::DynExpression],
        mut state: lisa::interpreter::State,
        cl: usize,
        steps: &mut lisa::interpreter::Steps,
    ) -> Result<lisa::interpreter::State, Box<dyn Error>> {
        let gil = Python::acquire_gil();
        let py = gil.python();
        let method: pyo3::PyObject = self.inner.getattr(py, method_name).map_err(PyError)?;
        let content = state.lib_context.downcast_ref::<pyo3::PyObject>().unwrap();
        let content: pyo3::PyObject = content.call_method0(py, "clone").map_err(PyError)?;
        method
            .call1(py, (content.clone_ref(py),))
            .map_err(PyError)?;
        state.lib_context = Rc::new(content);
        steps.push(cl, state.clone());
        Ok(state)
    }
    fn create_context(&self) -> Rc<dyn Any> {
        let gil = Python::acquire_gil();
        let py = gil.python();
        match self.inner.getattr(py, "create_context") {
            Err(_err) => unreachable!(),
            Ok(method) => Rc::new(method.call0(py).unwrap()),
        }
    }
    fn duplicate_context(&self, context: &dyn Any) -> Rc<dyn Any> {
        match context.downcast_ref::<pyo3::PyObject>() {
            Some(obj) => {
                let gil = Python::acquire_gil();
                let py = gil.python();
                Rc::new(obj.call_method0(py, "clone").unwrap())
            }
            _ => unreachable!(),
        }
    }
}

/// This module is a python module implemented in Rust.
#[pymodule]
fn lisa(py: Python, m: &PyModule) -> PyResult<()> {
    m.add_class::<Interpreter>()?;
    m.add_class::<Instruction>()?;
    m.add_class::<State>()?;
    m.add("SyntaxError", py.get_type::<SyntaxError>())?;
    m.add("RuntimeError", py.get_type::<RuntimeError>())?;
    Ok(())
}
