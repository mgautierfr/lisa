import pytest
import lisa


wrong_syntax = [
"create var", # Missing name of the variable
"a =", # Missing value
"if", # Missing test
"loop", # Missing a lot of things
"loop x", # Missing the "time"
"loop times", # Missing the value
"if 10", # Not a test
"loop 5 == 5 times", # Not a value
"a", # Not an instruction
"5*2", # Not an instruction
"if (5==10", # Missing the )
"if 5==10)", # Missing the (
"if 5==5\na=2", # Wrong indentiation
"a=2\n  b=5", # Wrong indentiation
]
@pytest.fixture(params=wrong_syntax)
def wrong_syntax(request):
    return request.param


def test_syntaxerror(wrong_syntax):
    with pytest.raises(lisa.SyntaxError):
       instructions = lisa.Instruction.parse(wrong_syntax)


runtime_error = [
"a = 5", # Unknown a
"create var a\ncreate var a", # Twice a
"foo()", # Unknown builtin call
"create var a\nif a\n  a = 5", # Unset var a
"create var a\nloop a times\n  a = 5", # Unset var a
"if a\n  a = 5", # Missing variable a
"create var a\ncreate var b\na = b * 2", # Unset var b
"create var a\na = b * 2", # Missing variable b
]
@pytest.fixture(params=runtime_error)
def runtime_error(request):
    return request.param

def test_runtime_error_syntax(runtime_error):
    lisa.Instruction.parse(runtime_error)

def test_runtime_error(runtime_error):
    instructions = lisa.Instruction.parse(runtime_error)
    interpreter = lisa.Interpreter(None)
    with pytest.raises(lisa.RuntimeError):
        state = interpreter.run(instructions)

def test_noclone_innamespace():
    class SimpleLibrary:
        def create_context(self):
            return {'a': 0}

        def inc(self, context):
            context['a'] += 1

    instructions = lisa.Instruction.parse("inc()\ninc()")
    interpreter = lisa.Interpreter(SimpleLibrary())
    with pytest.raises(Exception):
        state = interpreter.run(instructions)



