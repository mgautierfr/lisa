use crate::expression::DynExpression;
use crate::interpreter::{State, Steps};
use std::any::Any;
use std::error::Error;
use std::rc::Rc;

pub trait Library {
    fn run_method(
        &self,
        method_name: &str,
        arguments: &[DynExpression],
        state: State,
        cl: usize,
        steps: &mut Steps,
    ) -> Result<State, Box<dyn Error>>;
    fn create_context(&self) -> Rc<dyn Any>;
    fn duplicate_context(&self, context: &dyn Any) -> Rc<dyn Any>;
}

pub mod printlib;
