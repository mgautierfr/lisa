use crate::expression::*;
use crate::instruction::*;
use pest::iterators::Pair;
use pest::Parser;
use std::collections::VecDeque;
use std::error::Error;
use std::fmt;

#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct LisaParser;

/// The line is the container of a instruction in a program.
#[derive(Debug)]
pub struct Line {
    pub text: String,
    pub lineno: usize,
}

pub type ParsingError = pest::error::Error<Rule>;

#[derive(Debug)]
pub struct BlockError {
    pub text: String,
}
impl fmt::Display for BlockError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.text)
    }
}

#[derive(Debug)]
pub enum ErrorKind {
    ParsingError(ParsingError),
    BlockError(BlockError),
}

#[derive(Debug)]
pub struct SyntaxError {
    pub line: Line,
    pub error: ErrorKind,
}
impl SyntaxError {
    pub fn new_from_parsing(line: Line, err: ParsingError) -> Self {
        SyntaxError {
            line,
            error: ErrorKind::ParsingError(err),
        }
    }
    pub fn new_from_block(line: Line, err: BlockError) -> Self {
        SyntaxError {
            line,
            error: ErrorKind::BlockError(err),
        }
    }
}
impl fmt::Display for SyntaxError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.error {
            ErrorKind::ParsingError(err) => write!(f, "Line {} : {}", self.line.lineno, err),
            ErrorKind::BlockError(err) => write!(f, "BlockError : {}", err),
        }
    }
}
impl Error for SyntaxError {}

fn process_pair_to_instruction(pair: Pair<Rule>) -> InstructionKind {
    match pair.as_rule() {
        Rule::assignement => {
            let mut pairs = pair.into_inner();
            let identifier = pairs.next().unwrap().as_str();
            let expr = process_pair_to_expression(pairs.next().unwrap());
            InstructionKind::SetValue(identifier.to_string(), expr)
        }
        Rule::var_declaration => {
            let mut pairs = pair.into_inner();
            let _var_type = pairs.next().unwrap().as_str();
            let identifier = pairs.next().unwrap().as_str();
            InstructionKind::CreateVariable(identifier.to_string())
        }
        Rule::builtincall => {
            let mut pairs = pair.into_inner();
            let identifier = pairs.next().unwrap().as_str();
            let mut arguments = Vec::new();
            for p in pairs {
                arguments.push(process_pair_to_expression(p));
            }
            InstructionKind::BuiltinCall(identifier.to_string(), arguments)
        }
        Rule::ifstmt => {
            let expr = process_pair_to_expression(pair.into_inner().next().unwrap());
            InstructionKind::Conditional(expr, None)
        }
        Rule::loopstmt => {
            let expr = process_pair_to_expression(pair.into_inner().next().unwrap());
            InstructionKind::Conditional(expr, None)
        }
        Rule::comment => InstructionKind::Skip,
        _ => unreachable!(),
    }
}

fn process_pair_to_expression(pair: Pair<Rule>) -> DynExpression {
    match pair.as_rule() {
        Rule::expression | Rule::term | Rule::paren => {
            process_pair_to_expression(pair.into_inner().next().unwrap())
        }
        Rule::literal => Box::new(Literal::new(pair.as_str())),
        Rule::identifier => Box::new(Identifier::new(pair.as_str())),
        Rule::binaryOp => {
            let mut pairs = pair.into_inner();
            let lh = process_pair_to_expression(pairs.next().unwrap());
            let op = pairs.next().unwrap().as_str();
            let rh = process_pair_to_expression(pairs.next().unwrap());
            match op {
                "+" | "-" | "/" | "*" => {
                    let op_kind = match op {
                        "+" => OperatorKind::ADD,
                        "-" => OperatorKind::SUB,
                        "/" => OperatorKind::DIV,
                        "*" => OperatorKind::MUL,
                        _ => unreachable!(),
                    };
                    Box::new(Operator::new(op_kind, lh, rh))
                }
                "==" | "<" | ">" | "!=" | "<=" | ">=" => {
                    let bop_kind = match op {
                        "==" => BOperatorKind::EQ,
                        "!=" => BOperatorKind::NEQ,
                        "<" => BOperatorKind::LT,
                        ">" => BOperatorKind::GT,
                        "<=" => BOperatorKind::LTE,
                        ">=" => BOperatorKind::GTE,
                        _ => unreachable!(),
                    };
                    Box::new(BOperator::new(bop_kind, lh, rh))
                }
                _ => unreachable!(),
            }
        }
        _ => unreachable!(),
    }
}

type PreParsedInstruction = (Line, usize, InstructionKind);

fn parse(line: Line) -> Result<PreParsedInstruction, SyntaxError> {
    let pair = match LisaParser::parse(Rule::instruction, &line.text) {
        Ok(mut iter) => iter.next().unwrap(),
        Err(error) => return Err(SyntaxError::new_from_parsing(line, error)),
    };

    let mut pairs = pair.into_inner();
    let indentspan = pairs.next().unwrap().as_span();
    let indent = indentspan.end() - indentspan.start();

    let pair = pairs.next().unwrap();
    let instruction_kind = match pair.as_rule() {
        Rule::EOI => InstructionKind::Skip,
        _ => process_pair_to_instruction(pair),
    };
    Ok((line, indent, instruction_kind))
}

pub fn parse_source(source: &str) -> Result<Option<Box<Instruction>>, SyntaxError> {
    let instructions: Result<VecDeque<PreParsedInstruction>, _> = source
        .lines()
        .enumerate()
        .filter_map(|(lineno, text)| {
            match parse(Line {
                lineno,
                text: text.to_string(),
            }) {
                Err(e) => Some(Err(e)),
                Ok((_line, _i, kind)) => {
                    if let InstructionKind::Skip = kind {
                        None
                    } else {
                        Some(Ok((_line, _i, kind)))
                    }
                }
            }
        })
        .collect();

    build_instruction(&mut instructions?, 0)
}

macro_rules! check_instruction_indent {
    ( $instruction:ident, $indent:ident, $line:ident ) => {
        if $instruction.is_some() && $instruction.as_ref().unwrap().get_indent() > $indent {
            return Err(SyntaxError::new_from_block(
                $line,
                BlockError {
                    text: "Unexpected indent".to_string(),
                },
            ));
        }
    };
}

fn build_from_next(
    pre_parsed: PreParsedInstruction,
    instructions: &mut VecDeque<PreParsedInstruction>,
) -> Result<Box<Instruction>, SyntaxError> {
    let (cline, cindent, ckind) = pre_parsed;
    match ckind {
        InstructionKind::Conditional(var, _inner) => {
            let child_instruction = build_instruction(instructions, cindent + 1)?;
            if child_instruction.is_none() {
                Err(SyntaxError::new_from_block(
                    cline,
                    BlockError {
                        text: "Missing indented instruction".to_string(),
                    },
                ))
            } else {
                let next_instruction = build_instruction(instructions, cindent)?;
                check_instruction_indent!(next_instruction, cindent, cline);
                Ok(Box::new(Instruction::new(
                    cline,
                    cindent,
                    InstructionKind::Conditional(var, child_instruction),
                    next_instruction,
                )))
            }
        }
        _ => {
            let next_instruction = build_instruction(instructions, cindent)?;
            check_instruction_indent!(next_instruction, cindent, cline);
            Ok(Box::new(Instruction::new(
                cline,
                cindent,
                ckind,
                next_instruction,
            )))
        }
    }
}

fn build_instruction(
    instructions: &mut VecDeque<PreParsedInstruction>,
    min_indent: usize,
) -> Result<Option<Box<Instruction>>, SyntaxError> {
    Ok(
        if instructions.is_empty() || instructions.front().unwrap().1 < min_indent {
            None
        } else {
            Some(build_from_next(
                instructions.pop_front().unwrap(),
                instructions,
            )?)
        },
    )
}
#[cfg(test)]
mod tests {
    use super::*;

    macro_rules! parsing_instruction_should_fail {
        ( $( ($name:ident, $text:expr) ),+ ) => {
            $(#[test]
            fn $name() {
                let l = Line {
                    text: $text.to_string(),
                    lineno: 0
                };
                parse(l).expect_err("This should fail");
            })+
        }
    }

    parsing_instruction_should_fail!((simple_identifier, "a"), (simple2, "b"));

    macro_rules! parsing_instruction_should_succeed {
        ( $( ($name:ident, $text:expr) ),+ ) => {
            $(#[test]
            fn $name() {
                let l = Line {
                    text: $text.to_string(),
                    lineno: 0
                };
                parse(l).expect("This should parse");
            })+
        }
    }

    parsing_instruction_should_succeed!(
        (create0, "create var a"),
        (create1, "create var long_variable_name"),
        (parse0, "a = 5"),
        (parse1, "a = b"),
        (parse2, "a = 5 + 6"),
        (parse3, "a = 5 - 6"),
        (parse4, "a = 5/6"),
        (parse5, "a = 5*6"),
        (parse6, "a = a < 5"),
        (parse7, "a = a <= 5"),
        (parse8, "a = a >= 5"),
        (parse9, "a = a == 5"),
        (parse10, "a = a != 5"),
        (parse11, "a = (5+5)*2/b")
    );

}
